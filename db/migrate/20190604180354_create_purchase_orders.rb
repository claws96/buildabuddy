class CreatePurchaseOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :purchase_orders do |t|
      t.datetime :order_date
      t.float :total_cost, default: 0.0
      t.float :total_price, default: 0.0
      t.float :total_profit, default: 0.0
      t.timestamps
    end
  end
end
