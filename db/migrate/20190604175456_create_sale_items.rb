class CreateSaleItems < ActiveRecord::Migration[5.2]
  def change
    create_table :sale_items do |t|
      t.string  :description
      t.string  :product_size
      t.integer :quantity
      t.float   :cost
      t.float   :sale_price
      t.string  :type
      t.integer :product_id, index: true
      t.string  :item_slug, index: true
      t.timestamps
    end
  end
end
