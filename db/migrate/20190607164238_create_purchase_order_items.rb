class CreatePurchaseOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :purchase_order_items do |t|
      t.integer :quantity
      t.string  :item_slug, index: true
      t.float   :unit_price
      t.float   :total_price
      t.float   :unit_cost
      t.float   :total_cost
      t.references :purchase_order, index: true
      t.timestamps
    end
  end
end
