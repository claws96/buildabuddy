desc 'Build a Buddy Data Management Tasks'
task 'import', [:workbook_name] => :environment do |t, args|
  s = StuffedAnimalDataReader.new args[:workbook_name]
  s.process

end

task 'max_price_query', [:max_price, :max_count] => :environment do |t, args|
  args.with_defaults(max_count: 3)
  obj = PurchaseOrder.max_price_query(args[:max_price], args[:max_count])

  # Hard to read on the screen, but perfect for CSV files.

  puts "Order id,Order Cost,Customer price,Profit"
  obj.each do |po|
    puts "#{po.id},#{po.total_order_cost},#{po.total_order_price},#{po.total_order_profit}"
  end
end
