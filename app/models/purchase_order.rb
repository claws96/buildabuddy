class PurchaseOrder < ActiveRecord::Base
  has_many :purchase_order_items
  accepts_nested_attributes_for :purchase_order_items

  def self.max_price_query(max_price, max_result_count = 3)
    # returns an array of PurchaseOrder objects, which individually
    # can yield the given order's PurchaseOrderItems
    sql = "select id, total_price as total_order_price,
                      total_cost as total_order_cost,
                      total_profit as total_order_profit
             from purchase_orders
             where total_order_price <= ?
             group by total_order_price
             order by total_order_profit DESC
             limit ?"

    find_by_sql [sql, max_price, max_result_count]
  end

end
