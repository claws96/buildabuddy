class PurchaseOrderItem < ActiveRecord::Base
  belongs_to :purchase_order
  has_one :sale_item, primary_key: "item_slug", foreign_key: "item_slug"

  scope :product_type, -> {includes(:sale_item).joins(:sale_item)}
  scope :products, -> {product_type.where("sale_items.type = ?", "Product")}
  scope :accessories, -> {product_type.where("sale_items.type = ?", "Accessory")}

  # Returns an array of product objects and accessory objects as appropriate.
  def self.related_items_query(item_slug, max_result_count = 3)
    sql = "select distinct poix.*
           from purchase_order_items poi, purchase_order_items poix
           where poi.item_slug = ?
           and poix.item_slug <> ?
           and poi.purchase_order_id = poix.purchase_order_id
           group by poix.item_slug
           order by poix.purchase_order_id
           limit ?"

    find_by_sql([sql, item_slug, item_slug, max_result_count])
  end


end
