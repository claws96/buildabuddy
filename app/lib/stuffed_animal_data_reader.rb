require 'rubyXL'

ITEMABLE_TYPES = ['Accessory', 'Product'].freeze

# Idea here is to make a sort of ORM for the XLS data.
class StuffedAnimalDataReader


  def initialize(workbook_name)
    @@workbook_name = workbook_name
  end

  def process

    i = InventoryReader.new
    product_data = i.stuffed_animal_data
    accessory_data = i.accessories_data

    puts "Inventory data for #{product_data.keys.length} products read."
    puts "Accessory data for #{accessory_data.keys.length} accessories read."

    p = ProductPriceReader.new
    product_data.deep_merge! p.stuffed_animal_data
    accessory_data.deep_merge! p.accessories_data

    ac = AccessoryCompatibilityReader.new
    accessory_compatibility_data = ac.compatibility_data

    product_data.each do |product_key, value|
      p = Product.new value
      accessory_compatibility_data[product_key].each do |accessory_key|
        a = Accessory.create(accessory_data[accessory_key])
        p.accessories << a
      end
      p.save
    end

    por = PurchaseOrderReader.new
    # contains nested PurchaseOrderItems
    por.purchase_order_data.each do |pod|
      po = PurchaseOrder.new pod
      po.save
    end

  end

  def workbook
    @workbook || open_workbook
  end

  def open_workbook
    @workbook = RubyXL::Parser.parse(@@workbook_name)
  end

end

