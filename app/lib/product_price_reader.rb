class ProductPriceReader < StuffedAnimalDataReader

  def initialize
    @product_price_sheet_name = 'Product Prices'
  end

  def stuffed_animal_data
    return_data = {}
    row = 2 # starting row, 0-based

    this_sheet = input_sheet
    while this_sheet[row] && this_sheet[row][0]
      # 0 = desc
      # 1 = cost
      # 2 = sale price
      key = this_sheet[row][0].value.parameterize
      return_data[key] = { cost: this_sheet[row][1].value,
                           sale_price: this_sheet[row][2].value,
                           item_slug: key }
      row += 1
    end

    return_data
  end

  def accessories_data
    return_data = {}
    row = 2 # starting row, 0-based

    this_sheet = input_sheet
    while this_sheet[row] && this_sheet[row][4]
      # 4 = description
      # 5 = size
      # 6 = cost
      # 7 = sale price
      key = "#{this_sheet[row][4].value.parameterize}-#{this_sheet[row][5].value.parameterize}"
      return_data[key] = { product_size: this_sheet[row][5].value,
                           cost: this_sheet[row][6].value,
                           sale_price: this_sheet[row][7].value,
                           item_slug: key }
      row += 1
    end

    return_data
  end

  def workbook
    super
  end

  def input_sheet
    workbook[@product_price_sheet_name]
  end

end

