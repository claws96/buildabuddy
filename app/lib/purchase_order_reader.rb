class PurchaseOrderReader < StuffedAnimalDataReader

  def initialize
    @purchase_order_price_sheet_name = 'Purchase Orders'
  end

  def purchase_order_data
    this_sheet = input_sheet
    row = 2
    col = initial_col = 2
    return_data = []
    order = {}
    itemable = nil

    while this_sheet[row] && this_sheet[row][0]
      break if this_sheet[row][0].value.nil?

      order = new_order(this_sheet[row][0].value.to_date,
                        this_sheet[row][1].value.strftime('%H:%M:%S'))

      while this_sheet[row][col]
        modifier = this_sheet[0][col].value.to_s unless this_sheet[0][col].value.blank?
        slug_1 = "#{this_sheet[1][col].value} #{modifier}".parameterize
        slug_2 = this_sheet[1][col].value.to_s.parameterize
        qty = this_sheet[row][col].value

        if qty > 0
          itemable = search_union([slug_1, slug_2])

          items_data = new_purchase_order_item(itemable, qty)

          order[:purchase_order_items] << items_data
          order[:total_price] += round_money items_data[:total_price]
          order[:total_cost] += round_money items_data[:total_cost]
          order[:total_profit] += round_money items_data[:total_price]
        end

        col += 1
      end

      col = initial_col
      row += 1
      return_data << order
    end

    return_data
  end

  def search_union(slug_array)
    columns = "id,item_slug,sale_price,cost"

    p = Product.select(columns).where(item_slug: slug_array).order(nil).first
    a = Accessory.select(columns).where(item_slug: slug_array).order(nil).first

    a || p
  end

  def new_purchase_order_item(itemable, qty)
    PurchaseOrderItem.new(
        quantity: qty,
        item_slug: itemable.item_slug,
        unit_price: round_money(itemable.sale_price.to_f),
        total_price: round_money(itemable.sale_price.to_f * qty),
        unit_cost: round_money(itemable.cost.to_f),
        total_cost: round_money(itemable.cost.to_f * qty))
  end

  def new_order(date, time)
    date_time = DateTime.strptime("#{date} #{time}", '%Y-%m-%d %H:%M:%S')

    { order_date: date_time,
      purchase_order_items: [],
      total_price: 0.0,
      total_cost: 0.0,
      total_profit: 0.0 }
  end

  def round_money(value)
    ((value.to_f * 100).round) / 100
  end

  def workbook
    super
  end

  def input_sheet
    workbook[@purchase_order_price_sheet_name]
  end

end

