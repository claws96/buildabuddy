class InventoryReader < StuffedAnimalDataReader

  def initialize
    @inventory_sheet_name = 'Inventory'
  end

  def stuffed_animal_data
    return_data = {}
    row = 2 # starting row, 0-based

    this_sheet = input_sheet
    while this_sheet[row] && this_sheet[row][0]
      # 0 = desc
      # 1 = quantity
      key = this_sheet[row][0].value.parameterize
      return_data[key] = { description: this_sheet[row][0].value,
                           quantity: this_sheet[row][1].value,
                           item_slug: key }
      row += 1
    end

    return_data
  end

  def accessories_data
    return_data = {}
    row = 2 # starting row, 0-based

    this_sheet = input_sheet
    while this_sheet[row] && this_sheet[row][3]
      # 3 = description
      # 4 = size
      # 5 = quantity
      key = "#{this_sheet[row][3].value.parameterize}-#{this_sheet[row][4].value.parameterize}"
      return_data[key] = { description: this_sheet[row][3].value,
                           product_size: this_sheet[row][4].value,
                           quantity: this_sheet[row][5].value,
                           item_slug: key }
      row += 1
    end

    return_data
  end

  def workbook
    super
  end

  def input_sheet
    workbook[@inventory_sheet_name]
  end
end
