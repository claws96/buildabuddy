class AccessoryCompatibilityReader < StuffedAnimalDataReader
  def initialize
    @accessory_compatibility_sheet_name = 'Accessory Compatibility'
  end

  def compatibility_data
    row = 2
    col = initial_col = 1
    this_sheet = input_sheet
    acc_names = accessory_names
    item_matrix = {}
    header_row = 1

    while this_sheet[row] && this_sheet[row][0]
      item_name = this_sheet[row][0].value.parameterize
      item_matrix[item_name] ||= []

      while this_sheet[header_row][col]
        item_matrix[item_name] << acc_names[col - 1] unless this_sheet[row][col].nil? || this_sheet[row][col].blank?
        col += 1
      end

      row += 1
      col = initial_col
    end

    item_matrix
  end

  def accessory_names
    col = 1
    product_size = ''
    return_data = []
    this_sheet = input_sheet

    while this_sheet[0] && this_sheet[0][col]
      product_size = this_sheet[0][col].value || product_size
      return_data << "#{this_sheet[1][col].value}-#{product_size}".parameterize

      col += 1
    end

    return_data
  end

  def workbook
    super
  end

  def input_sheet
    workbook[@accessory_compatibility_sheet_name]
  end
end
