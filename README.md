# README

Buildabuddy implementation, Paul McCubbins

Had some interruptions and hardware problems, worked over the course of a couple of days:
6/4: 12:54 - 14:30
6/5: 08:51 - 10:30, 12:37 - 1:32

- Seems to need some kind of SKU or unique sluggable identifier.  Using "Description" is terrible.
- Didn't get a chance to implement the Purchase Orders table.  Keeping track of the total build cost and total build
  price would give an opportunity to implement the desired "closest but not exceeding price" with a simple sql query.
  The query could include the necessary calculations.
